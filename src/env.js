const dotenv = require('dotenv');
const path = require('path');

if (!process.env.NODE_ENV) {
  throw new Error('The NODE_ENV environment variable is required but was not specified.');
}

const envPath = path.join(__dirname, '..', '.env');

dotenv.config({ path: envPath });