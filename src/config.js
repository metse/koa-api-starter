import './env';

const config = {
  environment: process.env.NODE_ENV,
  port: process.env.APP_PORT,
};

export default config;