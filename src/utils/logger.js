import winston from 'winston';
import moment from 'moment';

const format = winston.format.printf((info) => {
  return `${info.level}: ${info.message} (${moment().format('YYYY-MM-DDTHH:mm:ss.SSSZZ')})`;
});

const Logger = winston.createLogger({
  transports: [
    new winston.transports.Console({
      format: winston.format.combine(winston.format.colorize(), format),
    }),
  ],
});

export default {
  info: message => Logger.log({ level: 'info', message }),
  error: message => Logger.log({ level: 'error', message }),
};
