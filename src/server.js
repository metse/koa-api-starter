import Koa from 'koa';
import logger from 'koa-logger';
import helmet from 'koa-helmet'; 
import cors from '@koa/cors';
import config from './config';
import Logger from './utils/logger';

const nodePort = config.port || 3001;
const app = new Koa();

app.proxy = true;

export const server = app
  .use(cors())
  .use(logger())
  .use(helmet())
  .listen(nodePort);

Logger.info(`App listening on port ${nodePort} in ${config.environment}`);
